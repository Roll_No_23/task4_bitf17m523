#include<stdio.h>
#include<stdlib.h>
#define MAXCHAR 10

void main(int argc, char **argv){
	FILE *fp;
	char str[MAXCHAR];
	int arrival_time[MAXCHAR],burst_time[MAXCHAR],temp[MAXCHAR];
	int wait_time = 0, turnaround_time = 0;
	int t_slice = atoi(argv[2]);
	int process_count = 0;
	int x = 0,counter = 0;
	int y = 0;
	float average_wait_time = 0.0, average_turnaround_time = 0.0;

	int limit = process_count;

	//Test to see if arguements works along with opening file
	//printf("%d",t_slice);
	//printf("\n");
	//printf("%s",argv[1]);
	//printf("%s",argv[2]);
 
    fp = fopen(argv[1], "r");
    if (fp == NULL){
        printf("Could not open file %s",argv[1]);
        exit(0);
    }
    	fscanf (fp, "%d %d", &x,&y);
  	for(int i = 0; !feof (fp); i++){
			arrival_time[i] = x;
			burst_time[i] = y;
			temp[i] = y;
	fscanf (fp, "%d %d", &x,&y);
	printf("%d %d\n",arrival_time[i],burst_time[i]);
	process_count++;
	}
    fclose(fp);

	//file reading test succesfull :)
	//printf("%d",process_count);
	printf("\nProcesses\t\tBurst Time\t Turnaround Time\t Waiting Time\n");
	x = process_count;
	for(int end = 0,y = 0; x != 0;){
		if(temp[y] <= t_slice && temp[y] > 0){
			end = end + temp[y];
			temp[y] = 0;
			counter = 1;
		}
		else if(temp[y] > 0){
			temp[y] = temp[y] - t_slice;
			end = end + t_slice;
		}

		if(temp[y] == 0 && counter == 1){
			x--;
			printf("\nProcess[%d]\t\t%d\t\t %d\t\t\t %d", y + 1, burst_time[y], end - arrival_time[y], end - arrival_time[y] - burst_time[y]);
                  wait_time = wait_time + end - arrival_time[y] - burst_time[y];
                  turnaround_time = turnaround_time + end - arrival_time[y];
                  counter = 0;
            
		}
		
		if(y == process_count - 1){
			y = 0;
		}
		else if(arrival_time[y + 1] <= end){
			y++;
		}
		else{
			y = 0;
		}
	}
      printf("\n");
}